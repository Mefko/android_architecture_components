package com.mefcorp.androidarchitecturecomponentstest.ActionLiveData;

import android.arch.lifecycle.ViewModel;

public class TestViewModel extends ViewModel {

	private ActionLiveData<Boolean> actionLiveData;

	public TestViewModel(){
		actionLiveData = new ActionLiveData<>();
	}

	public ActionLiveData<Boolean> getActionLiveData() {
		return actionLiveData;
	}
}
