package com.mefcorp.androidarchitecturecomponentstest.ActionLiveData;

import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.support.annotation.NonNull;

public class ActionLiveData<T> extends LiveData<T> {

	@Override
	public void observe(@NonNull LifecycleOwner owner, @NonNull Observer observer) {
		super.observe(owner, (value) -> {
			if (value != null) {
				observer.onChanged(value);
				setValue(null);
			}
		});
	}

	@Override
	public void observeForever(@NonNull Observer observer) {
		super.observeForever((value) -> {
			if (value != null) {
				observer.onChanged(value);
				setValue(null);
			}
		});
	}

	public void doAction(T value) {
		setValue(value);
	}

	public void postAction(T value) {
		postValue(value);
	}

}
