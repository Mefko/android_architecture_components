package com.mefcorp.androidarchitecturecomponentstest.aac.live_data;

import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.mefcorp.androidarchitecturecomponentstest.R;

/**
 * Created by illyu on 25.03.2018.
 */

public class LiveDataDefaultActivity extends AppCompatActivity implements View.OnClickListener, Observer<String> {

	private MutableLiveData<String> mFirstData;
	private MutableLiveData<String> mSecondData;
	private MediatorLiveData<String> mMediatorLiveData;
	private TextView mTextView;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_leve_data_default);
		findViewById(R.id.live_data_default_first).setOnClickListener(this);
		findViewById(R.id.live_data_default_second).setOnClickListener(this);
		findViewById(R.id.live_data_default_mediator).setOnClickListener(this);
		mTextView = findViewById(R.id.live_data_default_text);
		mFirstData = new MutableLiveData<>();
		mSecondData = new MutableLiveData<>();
		mMediatorLiveData = new MediatorLiveData<>();
		mMediatorLiveData.addSource(mFirstData, (s) -> {
			Log.d("LiveData", "first: " + s);
			mMediatorLiveData.setValue(s);

		});
		mMediatorLiveData.addSource(mSecondData, (s) -> {
			Log.d("LiveData", "Second: " + s);
			mMediatorLiveData.setValue(s);
		});
		mMediatorLiveData.observe(this, this);

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.live_data_default_first:
				mFirstData.setValue("Press first");
				break;
			case R.id.live_data_default_second:
				mSecondData.setValue("Press second");
				break;
			case R.id.live_data_default_mediator:
				mMediatorLiveData.setValue("Press mediator");
				break;
		}
	}

	@Override
	public void onChanged(@Nullable String s) {
		Log.d("LiveData", "Mediator: " + s);
		mTextView.setText(s);
	}
}
