package com.mefcorp.androidarchitecturecomponentstest.aac.view_model;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mefcorp.androidarchitecturecomponentstest.R;

/**
 * Created by illyu on 25.03.2018.
 */

public class ViewModelFragment extends Fragment implements View.OnClickListener{

	private TextView mTextView;
	private DataHolderActivity mData;
	private DataHolderActivity mDataFromActivity;

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.frg_view_model, null);
		mTextView = view.findViewById(R.id.frg_view_model_text);
		mData = ViewModelProviders.of(this).get(DataHolderActivity.class);
		mData.getSomeData().observe(this, (text) -> mTextView.setText(text));

		view.findViewById(R.id.frg_view_model_set_data_to_act).setOnClickListener(this);
		final TextView textView = view.findViewById(R.id.frg_view_model_from_activity);
		mDataFromActivity = ViewModelProviders.of(getActivity()).get(DataHolderActivity.class);
		mDataFromActivity.getSomeData().observe(this, (text) -> textView.setText(text));
		return view;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
			case R.id.frg_view_model_set_data_to_act:
				mDataFromActivity.setData("Set from fragment");
				break;
		}
	}
}
