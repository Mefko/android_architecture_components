package com.mefcorp.androidarchitecturecomponentstest.aac;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.mefcorp.androidarchitecturecomponentstest.R;
import com.mefcorp.androidarchitecturecomponentstest.aac.lifecycle.LifecycleAAC;
import com.mefcorp.androidarchitecturecomponentstest.aac.lifecycle.LifecycleUsual;
import com.mefcorp.androidarchitecturecomponentstest.aac.live_data.LiveDataActivity;
import com.mefcorp.androidarchitecturecomponentstest.aac.live_data.LiveDataDefaultActivity;
import com.mefcorp.androidarchitecturecomponentstest.aac.view_model.ViewModelActivity;

public class AACActivity extends AppCompatActivity implements View.OnClickListener {

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle("Architecture components testing");
		setContentView(R.layout.act_acc);
		findViewById(R.id.lifeCycle).setOnClickListener(this);
		findViewById(R.id.lifecycleAAC).setOnClickListener(this);
		findViewById(R.id.liveDataCustom).setOnClickListener(this);
		findViewById(R.id.liveDataDefault).setOnClickListener(this);
		findViewById(R.id.viewModel).setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		Intent intent = null;
		switch (v.getId()) {
			case R.id.lifeCycle:
				intent = new Intent(this, LifecycleUsual.class);
				break;
			case R.id.lifecycleAAC:
				intent = new Intent(this, LifecycleAAC.class);
				break;
			case R.id.liveDataCustom:
				intent = new Intent(this, LiveDataActivity.class);
				break;
			case R.id.liveDataDefault:
				intent = new Intent(this, LiveDataDefaultActivity.class);
				break;
			case R.id.viewModel:
				intent = new Intent(this, ViewModelActivity.class);
				break;
			default:
				break;
		}
		if (intent != null)
			startActivity(intent);
	}
}
