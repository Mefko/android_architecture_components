package com.mefcorp.androidarchitecturecomponentstest.aac.live_data;

import android.arch.lifecycle.Observer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.mefcorp.androidarchitecturecomponentstest.R;



/**
 * Created by illyu on 25.03.2018.
 */

public class LiveDataActivity extends AppCompatActivity implements Observer<String> {

	private TextView mTextView;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_live_data);
		mTextView = findViewById(R.id.live_data_text);
		//subscribe on create activity and unsubscribe when activity in pause
		DataHolder.getInstance().observe(this, this);
	}

	@Override
	public void onChanged(@Nullable String s) {
		mTextView.setText(s);
	}
}
