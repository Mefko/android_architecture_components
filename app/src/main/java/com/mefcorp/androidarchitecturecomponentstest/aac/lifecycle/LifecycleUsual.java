package com.mefcorp.androidarchitecturecomponentstest.aac.lifecycle;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.mefcorp.androidarchitecturecomponentstest.R;

/**
 * Created by illyu on 25.03.2018.
 */

public class LifecycleUsual extends AppCompatActivity {

	private DoSome mDoSOme;
	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_life_cycle);
		mDoSOme = new DoSome(this);
	}

	@Override
	protected void onStart() {
		super.onStart();
		mDoSOme.onStart();
	}

	@Override
	protected void onPause() {
		mDoSOme.onPause();
		super.onPause();
	}

	private static class DoSome {

		private Context mContext;

		DoSome(Context context){
			mContext = context;
		}

		public void onStart(){
			Log.d("Lifecycle", "onStart");
			Toast.makeText(mContext, "onStart", Toast.LENGTH_SHORT).show();
		}

		public void onPause(){
			Log.d("Lifecycle", "onPause");
			Toast.makeText(mContext, "onPause", Toast.LENGTH_SHORT).show();
		}

	}
}
