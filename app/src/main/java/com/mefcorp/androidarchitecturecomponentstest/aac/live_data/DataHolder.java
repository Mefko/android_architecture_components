package com.mefcorp.androidarchitecturecomponentstest.aac.live_data;

import android.arch.lifecycle.LiveData;
import android.util.Log;

/**
 * Created by illyu on 25.03.2018.
 */

public class DataHolder extends LiveData<String> {

	private static DataHolder INSTANCE;

	public static DataHolder getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new DataHolder();
		}
		return INSTANCE;
	}

	private Thread mThread;

	//work only with application context
	private DataHolder(){
		mThread = new Thread(()->{
			int d = 1000;
			for(int i = 1; i < d; i++) {
				postValue(String.valueOf(i));
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});
	}

	@Override
	protected void onActive() {
		super.onActive();
		if(!mThread.isAlive()){
			mThread.start();
		}
		Log.d("LiveData", "Has subs");
	}

	@Override
	protected void onInactive() {
		super.onInactive();
		Log.d("LiveData", "Has't subs");
	}
}



