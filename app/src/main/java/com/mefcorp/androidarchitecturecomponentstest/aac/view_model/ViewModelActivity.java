package com.mefcorp.androidarchitecturecomponentstest.aac.view_model;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.mefcorp.androidarchitecturecomponentstest.R;

/**
 * Created by illyu on 25.03.2018.
 */

public class ViewModelActivity extends AppCompatActivity implements View.OnClickListener {

	private TextView mTextView;
	private DataHolderActivity mData;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_view_model);
		mTextView = findViewById(R.id.view_model_text);
		findViewById(R.id.view_model_add_remove_frg).setOnClickListener(this);
		findViewById(R.id.act_view_model_set_value).setOnClickListener(this);
		mData = ViewModelProviders.of(this).get(DataHolderActivity.class);
		mData.getSomeData().observe(this, (text) -> mTextView.setText(text));

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.view_model_add_remove_frg:
				FragmentManager fm = getSupportFragmentManager();
				Fragment fragment = fm.findFragmentByTag(ViewModelFragment.class.getSimpleName());
				if (fragment == null) {
					fragment = new ViewModelFragment();
					fm.beginTransaction().replace(R.id.view_model_frg_holder, fragment, ViewModelFragment.class.getSimpleName()).commit();
				} else {
					fm.beginTransaction().remove(fragment).commit();
				}
				break;
			case R.id.act_view_model_set_value:
				mData.setData("Set from activity");
				break;
		}
	}
}
