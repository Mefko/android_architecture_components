package com.mefcorp.androidarchitecturecomponentstest.aac.lifecycle;

import android.arch.lifecycle.*;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.mefcorp.androidarchitecturecomponentstest.R;

/**
 * Created by illyu on 25.03.2018.
 */

public class LifecycleAAC extends AppCompatActivity {

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_life_cycle);
		new DoSome(this, this.getLifecycle());
	}


	private static class DoSome implements LifecycleObserver {

		private Context mContext;

		DoSome(Context context, Lifecycle lifecycle){
			lifecycle.addObserver(this);
			mContext = context;
		}

		@OnLifecycleEvent(Lifecycle.Event.ON_START)
		public void onStart(){
			Log.d("Lifecycle", "onStart");
			Toast.makeText(mContext, "onStart", Toast.LENGTH_SHORT).show();
		}

		@OnLifecycleEvent(Lifecycle.Event.ON_STOP)
		public void onPause(){
			Log.d("Lifecycle", "onPause");
			Toast.makeText(mContext, "onPause", Toast.LENGTH_SHORT).show();
		}

	}
}
