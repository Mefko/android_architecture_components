package com.mefcorp.androidarchitecturecomponentstest.aac.view_model;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.util.Log;

/**
 * Created by illyu on 25.03.2018.
 */

public class DataHolderActivity extends ViewModel {

	private MutableLiveData<String> mSomeData;

	//must be public without parameters
	public DataHolderActivity() {
		mSomeData = new MutableLiveData<>();
		mSomeData.setValue("Just created");
	}

	public void setData(String value){
		mSomeData.setValue(value);
	}

	public MutableLiveData<String> getSomeData(){
		return mSomeData;
	}


	@Override
	protected void onCleared() {
		super.onCleared();
		Log.d("ViewModel", "Clean: " + DataHolderActivity.class.getSimpleName());
	}
}
