package com.mefcorp.androidarchitecturecomponentstest.architecture_patterns.mvvm;

import android.databinding.ObservableField;

public class AddNode {

	public ObservableField<String> addNode = new ObservableField<>();
	public ObservableField<Boolean> displaySome = new ObservableField<>();
}
