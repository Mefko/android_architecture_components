package com.mefcorp.androidarchitecturecomponentstest.architecture_patterns.mvp;

import android.text.TextUtils;

public class Presenter implements MVPActivityContract.Presenter {

	private MVPActivityContract.Model mModel;
	private MVPActivityContract.View mView;

	public Presenter(MVPActivityContract.Model model, MVPActivityContract.View view) {
		mModel = model;
		mView = view;
	}

	@Override
	public void init() {
		mView.setViewTitle("MVP");
		mView.setNodeList(mModel.getNodeList());
	}

	@Override
	public void addNode(String node) {
		if (!TextUtils.isEmpty(node)) {
			mModel.addNode(node);
			mView.setNodeList(mModel.getNodeList());
		}
	}

	@Override
	public void itemSelected(int position) {
		mView.showMessage(mModel.getNodeList().get(position));
	}

	@Override
	public void itemLongClick(int position) {
		if (position >= 0 && mModel.getNodeList().size() < position) {
			mModel.removeNode(position);
			mView.setNodeList(mModel.getNodeList());
		}
	}
}
