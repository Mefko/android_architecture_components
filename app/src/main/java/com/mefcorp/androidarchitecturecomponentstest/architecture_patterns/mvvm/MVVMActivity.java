package com.mefcorp.androidarchitecturecomponentstest.architecture_patterns.mvvm;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.widget.Toast;

import com.mefcorp.androidarchitecturecomponentstest.R;
import com.mefcorp.androidarchitecturecomponentstest.databinding.NodeListBinding;

public class MVVMActivity extends AppCompatActivity {

	private MVVMNodeAdapter mAdapter;
	private NodeListBinding mBinding;
	private MVVMModel mModel;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mvvm2);
		mModel = ViewModelProviders.of(this).get(MVVMModel.class);
		mAdapter = new MVVMNodeAdapter(this, mModel.getNodes());
		mBinding = DataBindingUtil.setContentView(this, R.layout.node_list);
		mBinding.nodeListList.setLayoutManager(new LinearLayoutManager(this));
		mBinding.nodeListList.setAdapter(mAdapter);
		mAdapter.setOnItemClickListener((p) -> Toast.makeText(this, mModel.getNodes().get(p), Toast.LENGTH_SHORT).show());
		mAdapter.setOnItemLongClickListener((p) -> {
			mModel.removeNode(p);
			mAdapter.notifyDataSetChanged();
			return true;
		});
		mBinding.nodeListAddButton.setOnClickListener((v) -> {
			String value = mBinding.nodeListTypedNode.getText().toString();
			mModel.addNode(value);
			mAdapter.notifyDataSetChanged();
		});
	}
}
