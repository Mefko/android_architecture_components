package com.mefcorp.androidarchitecturecomponentstest.architecture_patterns.mvvm;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mefcorp.androidarchitecturecomponentstest.R;
import com.mefcorp.androidarchitecturecomponentstest.databinding.ItemMvvmNodeBinding;

import java.util.List;

public class MVVMNodeAdapter extends RecyclerView.Adapter<MVVMNodeAdapter.ViewHolder> {

	private List<String> mList;
	private final LayoutInflater mInflater;
	private final Context mContext;
	private OnItemClick mOnItemClick;
	private OnItemLongClick mOnItemLongClick;
//	private NodeI

	public MVVMNodeAdapter(@NonNull Context context, List<String> list) {
		mInflater = LayoutInflater.from(context);
		mContext = context;
		mList = list;
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View view = mInflater.inflate(R.layout.item_mvvm_node, null);
		return new ViewHolder(view);
	}

	@Override
	public void onBindViewHolder(ViewHolder holder, final int position) {
		holder.bindItem(mList.get(position));
		holder.itemView.setOnClickListener((v) -> {
			if (mOnItemClick != null) {
				mOnItemClick.onItemClick(position);
			}
		});
		holder.itemView.setOnLongClickListener((v) -> {
			if (mOnItemLongClick != null) {
				return mOnItemLongClick.onItemLongClick(position);
			} else {
				return false;
			}
		});
	}

	@Override
	public int getItemCount() {
		return mList == null ? 0 : mList.size();
	}

	public void setOnItemClickListener(OnItemClick listener) {
		mOnItemClick = listener;
	}

	public void setOnItemLongClickListener(OnItemLongClick listener) {
		mOnItemLongClick = listener;
	}

	public static class ViewHolder extends RecyclerView.ViewHolder {

		private ItemMvvmNodeBinding mBinding;

		public ViewHolder(View itemView) {
			super(itemView);
			mBinding = DataBindingUtil.bind(itemView);
		}

		public void bindItem(String item) {
			mBinding.itemMvvmNodeText.setText(item);
		}
	}

	public interface OnItemClick {
		void onItemClick(int position);
	}

	public interface OnItemLongClick {
		boolean onItemLongClick(int position);
	}
}

