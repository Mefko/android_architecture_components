package com.mefcorp.androidarchitecturecomponentstest.architecture_patterns.mvp;

import java.util.List;

public class MVPActivityContract {

	interface View {

		void setViewTitle(String msg);

		void setNodeList(List<String> list);

		void showMessage(String msg);
	}

	interface Model {

		List<String> getNodeList();

		void addNode(String node);

		void removeNode(int position);
	}

	interface Presenter {

		void init();

		void addNode(String node);

		void itemSelected(int position);

		void itemLongClick(int position);
	}

}
