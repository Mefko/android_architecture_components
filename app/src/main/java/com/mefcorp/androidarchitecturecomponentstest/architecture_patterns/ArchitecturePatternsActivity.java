package com.mefcorp.androidarchitecturecomponentstest.architecture_patterns;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.mefcorp.androidarchitecturecomponentstest.R;
import com.mefcorp.androidarchitecturecomponentstest.architecture_patterns.mvp.MVPActivity;
import com.mefcorp.androidarchitecturecomponentstest.architecture_patterns.mvvm.MVVMActivity;
import com.mefcorp.androidarchitecturecomponentstest.architecture_patterns.without_patterns.WithoutPatternsActivity;

public class ArchitecturePatternsActivity extends AppCompatActivity implements View.OnClickListener {


	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle("Architecture patterns testing");
		setContentView(R.layout.act_architecture_patterns);
		findViewById(R.id.without_patterns).setOnClickListener(this::onClick);
		findViewById(R.id.mvp_pattern).setOnClickListener(this::onClick);
		findViewById(R.id.mvvm_pattern).setOnClickListener(this::onClick);
	}

	@Override
	public void onClick(View v) {
		Intent intent = null;
		switch (v.getId()) {
			case R.id.without_patterns:
				intent = new Intent(this, WithoutPatternsActivity.class);
				break;
			case R.id.mvp_pattern:
				intent = new Intent(this, MVPActivity.class);
				break;
			case R.id.mvvm_pattern:
				intent = new Intent(this, MVVMActivity.class);
				break;
		}

		if (intent != null) {
			startActivity(intent);
		}
	}
}
