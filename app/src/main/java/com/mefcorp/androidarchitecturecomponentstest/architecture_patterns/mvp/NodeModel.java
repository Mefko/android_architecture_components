package com.mefcorp.androidarchitecturecomponentstest.architecture_patterns.mvp;

import android.arch.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.List;

public class NodeModel extends ViewModel implements MVPActivityContract.Model {

	private ArrayList<String> mData = new ArrayList<>();

	@Override
	public List<String> getNodeList() {
		return mData;
	}

	@Override
	public void addNode(String node) {
		mData.add(node);
	}

	@Override
	public void removeNode(int position) {
		mData.remove(position);
	}
}
