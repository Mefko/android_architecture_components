package com.mefcorp.androidarchitecturecomponentstest.architecture_patterns.without_patterns;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.mefcorp.androidarchitecturecomponentstest.R;
import com.mefcorp.androidarchitecturecomponentstest.architecture_patterns.Data;

import java.util.ArrayList;

public class WithoutPatternsActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

	RecyclerView mListView;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_without_patterns);
		TextView textView1 = findViewById(R.id.act_without_patterns_textView1);
		textView1.setText("Some data 1");
		TextView textView2 = findViewById(R.id.act_without_patterns_textView2);
		textView2.setText("Some data 2");
		Spinner spinner = findViewById(R.id.act_without_patterns_spinner);
		spinner.setAdapter(new SimpleSpinnerAdapter(this, Data.getSpinnerData()));
		spinner.setOnItemSelectedListener(this);
		mListView = findViewById(R.id.act_without_patterns_list);
		mListView.setLayoutManager(new LinearLayoutManager(this));

	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
		Data.DataSpinnerStruct dataSpinnerStruct = (Data.DataSpinnerStruct) parent.getAdapter().getItem(position);
		mListView.setAdapter(new ListAdapter(dataSpinnerStruct.getMinValue(), dataSpinnerStruct.getMaxValue()));
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {

	}

	private static class SimpleSpinnerAdapter extends ArrayAdapter<Data.DataSpinnerStruct> {

		public SimpleSpinnerAdapter(@NonNull Context context, Data.DataSpinnerStruct[] data) {
			super(context, android.R.layout.simple_list_item_1, data);
		}

		@Override
		public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
			if (convertView == null) {
				convertView = LayoutInflater.from(getContext()).inflate(android.R.layout.simple_spinner_dropdown_item, parent, false);
			}
			((TextView) convertView.findViewById(android.R.id.text1)).setText(getItem(position).getName());
			return convertView;
		}

		@NonNull
		@Override
		public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
			return super.getView(position, convertView, parent);
		}
	}

	private static class ViewHolder extends RecyclerView.ViewHolder {

		public TextView mName1;
		public TextView mName2;

		public ViewHolder(View itemView) {
			super(itemView);
			mName1 = itemView.findViewById(R.id.item_list_adapter_text_view1);
			mName2 = itemView.findViewById(R.id.item_list_adapter_text_view2);

		}
	}

	public static class ListAdapter extends RecyclerView.Adapter<ViewHolder> {

		private ArrayList<Data.DataListStruct> mData;

		public ListAdapter(int minValue, int maxValue) {
			mData = Data.getData(minValue, maxValue);
		}

		@Override
		public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
			ViewHolder holder = new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_adapter, parent, false));
			return holder;
		}

		@Override
		public void onBindViewHolder(ViewHolder holder, int position) {
			Data.DataListStruct dataListStruct = mData.get(position);
			holder.mName1.setText(dataListStruct.getName1());
			holder.mName2.setText(dataListStruct.getName2());
		}

		@Override
		public int getItemCount() {
			return mData.size();
		}
	}
}

