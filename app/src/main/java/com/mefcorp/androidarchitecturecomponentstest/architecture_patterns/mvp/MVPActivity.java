package com.mefcorp.androidarchitecturecomponentstest.architecture_patterns.mvp;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.EditText;
import android.widget.Toast;

import com.mefcorp.androidarchitecturecomponentstest.R;

import java.util.List;

public class MVPActivity extends AppCompatActivity implements MVPActivityContract.View {


	private MVPActivityContract.Presenter mPresenter;
	private NodeAdapter mAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mvp);

		NodeModel model = ViewModelProviders.of(this).get(NodeModel.class);
		mPresenter = new Presenter(model, this);

		RecyclerView list = findViewById(R.id.mvp_activity_list);
		list.setHasFixedSize(false);
		list.setLayoutManager(new LinearLayoutManager(this));
		mAdapter = new NodeAdapter(this);
		list.setAdapter(mAdapter);
		mAdapter.setOnItemClickListener((position) -> mPresenter.itemSelected(position));
		mAdapter.setOnItemLongClickListener((position) -> {
			mPresenter.itemLongClick(position);
			return true;
		});
		final EditText typedNode = findViewById(R.id.mvp_activity_typed_node);
		findViewById(R.id.mvp_activity_add).setOnClickListener((v) -> mPresenter.addNode(typedNode.getText().toString()));


		mPresenter.init();
	}

	@Override
	public void setViewTitle(String msg) {
		setTitle(msg);
	}

	@Override
	public void setNodeList(List<String> list) {
		mAdapter.setList(list);
	}

	@Override
	public void showMessage(String msg) {
		Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
	}
}
