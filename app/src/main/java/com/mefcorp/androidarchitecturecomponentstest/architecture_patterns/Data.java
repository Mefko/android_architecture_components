package com.mefcorp.androidarchitecturecomponentstest.architecture_patterns;

import java.util.ArrayList;
import java.util.Random;

public class Data {


	public static DataSpinnerStruct[] getSpinnerData() {
		DataSpinnerStruct[] data = new DataSpinnerStruct[5];
		int prevMinValue = 0;
		int prevMaxValue = 0;
		for (int i = 0; i < data.length; i++) {
			prevMaxValue += 10;
			data[i] = new DataSpinnerStruct(prevMaxValue, prevMinValue, "From " + prevMinValue + " to " + prevMaxValue);
			prevMinValue += 10;
		}
		return data;
	}

	public static ArrayList<DataListStruct> getData(int minValue, int maxValue) {
		int count;
		if(maxValue == -1){
			count = minValue;
		} else {
			Random r = new Random();
			count = r.nextInt(maxValue - minValue) + minValue;
		}
		ArrayList<DataListStruct> dataListStructs = new ArrayList<>(count);
		for (int i = 1; i <= count; i++) {
			dataListStructs.add(new DataListStruct("row" + i, "item: " + i));
		}
		return dataListStructs;
	}

	public static class DataSpinnerStruct {
		private int mMaxValue;
		private int mMinValue;
		private String mName;

		public DataSpinnerStruct(int maxValue, int minValue, String name) {
			mMaxValue = maxValue;
			mMinValue = minValue;
			mName = name;
		}

		public int getMaxValue() {
			return mMaxValue;
		}

		public int getMinValue() {
			return mMinValue;
		}

		public String getName() {
			return mName;
		}

		@Override
		public String toString() {
			return getName();
		}
	}

	public static class DataListStruct {

		private String mName1;
		private String mName2;

		public DataListStruct(String s1, String s2) {
			mName1 = s1;
			mName2 = s2;
		}

		public String getName1() {
			return mName1;
		}

		public String getName2() {
			return mName2;
		}
	}
}
