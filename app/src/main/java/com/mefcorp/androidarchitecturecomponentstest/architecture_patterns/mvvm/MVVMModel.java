package com.mefcorp.androidarchitecturecomponentstest.architecture_patterns.mvvm;

import android.arch.lifecycle.ViewModel;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

public class MVVMModel extends ViewModel {

	private List<String> mData = new ArrayList<>();

	public List<String> getNodes() {
		return mData;
	}

	public void addNode(String node) {
		if (!TextUtils.isEmpty(node)) {
			mData.add(node);
		}
	}

	public void removeNode(int pos) {
		if (pos >= 0 && mData.size() < pos) {
			mData.remove(pos);
		}
	}
}
