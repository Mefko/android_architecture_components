package com.mefcorp.androidarchitecturecomponentstest.architecture_patterns.mvp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mefcorp.androidarchitecturecomponentstest.R;

import java.util.ArrayList;
import java.util.List;

public class NodeAdapter extends RecyclerView.Adapter<NodeAdapter.ViewHolder> {


	private List<String> mList;
	private final LayoutInflater mInflater;
	private OnItemClick mOnItemClick;
	private OnItemLongClick mOnItemLongClick;


	public NodeAdapter(@NonNull Context context) {
		mInflater = LayoutInflater.from(context);
		mList = new ArrayList<>();
		mList.add("add adapter");
	}


	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View view = mInflater.inflate(R.layout.item_node, null);
		return new ViewHolder(view);
	}

	@Override
	public void onBindViewHolder(ViewHolder holder, final int position) {
		holder.mTextView.setText(mList.get(position));
		holder.itemView.setOnClickListener((v) -> {
			if (mOnItemClick != null) {
				mOnItemClick.onItemClick(position);
			}
		});
		holder.itemView.setOnLongClickListener((v) -> {
			if (mOnItemLongClick == null) {
				return false;
			} else {
				return mOnItemLongClick.onItemLongClick(position);
			}
		});
	}

	@Override
	public int getItemCount() {
		return mList == null ? 0 : mList.size();
	}

	public void setList(List<String> list) {
		mList = list;
		notifyDataSetChanged();
	}

	public void setOnItemClickListener(OnItemClick listener) {
		mOnItemClick = listener;
	}

	public void setOnItemLongClickListener(OnItemLongClick listener) {
		mOnItemLongClick = listener;
	}


	public static class ViewHolder extends RecyclerView.ViewHolder {

		private TextView mTextView;

		public ViewHolder(View itemView) {
			super(itemView);
			mTextView = itemView.findViewById(R.id.ite_node_text);
		}
	}

	public interface OnItemClick {
		void onItemClick(int position);
	}

	public interface OnItemLongClick {
		boolean onItemLongClick(int position);
	}
}
