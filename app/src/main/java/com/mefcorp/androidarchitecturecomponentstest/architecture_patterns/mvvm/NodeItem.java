package com.mefcorp.androidarchitecturecomponentstest.architecture_patterns.mvvm;

import android.databinding.BaseObservable;
import android.databinding.ObservableField;

public class NodeItem extends BaseObservable {

	public final ObservableField<String> node = new ObservableField<>();
}
