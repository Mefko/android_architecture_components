package com.mefcorp.androidarchitecturecomponentstest;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.mefcorp.androidarchitecturecomponentstest.aac.AACActivity;
import com.mefcorp.androidarchitecturecomponentstest.architecture_patterns.ArchitecturePatternsActivity;
import com.mefcorp.androidarchitecturecomponentstest.for_test.ForTestActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle("Test application");
		setContentView(R.layout.activity_main);
		findViewById(R.id.acc_act).setOnClickListener(this);
		findViewById(R.id.architecturePatternsActivity).setOnClickListener(this);
		findViewById(R.id.testing).setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		Intent intent = null;
		switch (v.getId()) {
			case R.id.acc_act:
				intent = new Intent(this, AACActivity.class);
				break;
			case R.id.architecturePatternsActivity:
				intent = new Intent(this, ArchitecturePatternsActivity.class);
				break;
			case R.id.testing:
				intent = new Intent(this, ForTestActivity.class);
			default:
				break;
		}
		if (intent != null)
			startActivity(intent);
	}
}
