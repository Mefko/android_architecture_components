package com.mefcorp.androidarchitecturecomponentstest.for_unit_test;

import com.google.gson.Gson;

import java.io.IOException;

public class NameRepository1 {

	private final IFileReader mFileReader;

	public NameRepository1(IFileReader fileReader) {
		this.mFileReader = fileReader;
	}

	public String getName() throws IOException {
		Gson gson = new Gson();
		if(mFileReader.fileLength() > 0){
			User user = gson.fromJson(mFileReader.readData(), User.class);
			return user.name;
		}
		return "";
	}

	private static final class User {
		String name;
	}
}
