package com.mefcorp.androidarchitecturecomponentstest.for_unit_test;

import java.io.IOException;

public interface IFileReader {

	String readData() throws IOException;

	long fileLength();
}
