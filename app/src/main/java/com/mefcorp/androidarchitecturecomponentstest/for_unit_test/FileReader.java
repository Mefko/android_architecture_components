package com.mefcorp.androidarchitecturecomponentstest.for_unit_test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class FileReader implements IFileReader {

	private final File mFile;

	public FileReader(File file) {
		this.mFile = file;
	}

	@Override
	public String readData() throws IOException {
		byte[] bytes = new byte[(int) mFile.length()];
		try (FileInputStream in = new FileInputStream(mFile)) {
			in.read(bytes);
		}
		return new String(bytes, "UTF-8");
	}

	@Override
	public long fileLength() {
		return mFile.length();
	}
}
