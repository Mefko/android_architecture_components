package com.mefcorp.androidarchitecturecomponentstest.for_unit_test;

import com.google.gson.Gson;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class NameRepository {

	private final File mFile;

	public NameRepository(File file) {
		this.mFile = file;
	}

	public String getName() throws IOException {
		Gson gson = new Gson();
		User user = gson.fromJson(readFile(), User.class);
		return user.name;
	}

	public String readFile() throws IOException {
		byte[] bytes = new byte[(int) mFile.length()];
		try (FileInputStream in = new FileInputStream(mFile)) {
			in.read(bytes);
		}
		return new String(bytes, "UTF-8");
	}

	private static final class User {
		String name;
	}
}
