package com.mefcorp.androidarchitecturecomponentstest.for_test.mvvm.model;

import android.databinding.BaseObservable;
import android.databinding.ObservableField;

public class Item extends BaseObservable {

	public final ObservableField<String> someValue1 = new ObservableField<>();
	public final ObservableField<String> someValue2 = new ObservableField<>();

}
