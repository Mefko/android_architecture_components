package com.mefcorp.androidarchitecturecomponentstest.for_test.mvp_mosby;

import android.widget.TextView;

import com.hannesdorfmann.mosby.mvp.viewstate.lce.LceViewState;
import com.hannesdorfmann.mosby.mvp.viewstate.lce.MvpLceViewStateFragment;

public class MyLogic extends MvpLceViewStateFragment<TextView, MVPListPresenter.ForTestData, MVPListView, MVPListPresenter> {

	public void cancel() {

	}

	@Override
	public LceViewState<MVPListPresenter.ForTestData, MVPListView> createViewState() {
		return null;
	}

	@Override
	public MVPListPresenter.ForTestData getData() {
		return null;
	}

	@Override
	protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
		return null;
	}

	@Override
	public MVPListPresenter createPresenter() {
		return null;
	}

	@Override
	public void setData(MVPListPresenter.ForTestData data) {

	}

	@Override
	public void loadData(boolean pullToRefresh) {

	}
}
