package com.mefcorp.androidarchitecturecomponentstest.for_test.mvp_mosby;

import android.util.Log;

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

public class MVPPresenter extends MvpBasePresenter<MVPView> {

	private MyLogic logic;

	public MVPPresenter() {
		Log.e("MyF", "create presenter");
	}

	public void greetHello() {
		cancelIfRunning();
		if (isViewAttached()) {
			getView().showHello("Hello");
		}

	}

	public void greetGoodbye() {
		if (isViewAttached()) {
			getView().showGoodbye("Goodbye");
		}
	}

	private void cancelIfRunning() {
		if (logic != null) {
			logic.cancel();
		}
	}

	@Override
	public void detachView(boolean retainInstance) {
		super.detachView(retainInstance);
		if (retainInstance) {
			cancelIfRunning();
		}
	}
}
