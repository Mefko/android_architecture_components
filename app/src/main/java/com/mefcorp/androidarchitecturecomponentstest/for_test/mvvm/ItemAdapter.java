package com.mefcorp.androidarchitecturecomponentstest.for_test.mvvm;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

//import com.mefcorp.androidarchitecturecomponentstest.BR;
import com.mefcorp.androidarchitecturecomponentstest.R;
import com.mefcorp.androidarchitecturecomponentstest.for_test.mvvm.model.Item;
import com.mefcorp.androidarchitecturecomponentstest.databinding.ItemMvvmActivityBinding;

import java.util.List;

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ViewHolder> {

	private List<Item> mItems;
	private Context mCotext;
	private final LayoutInflater mInflater;

	public ItemAdapter(@NonNull Context context, @NonNull List<Item> items) {
		mItems = items;
		mCotext = context;
		mInflater = LayoutInflater.from(mCotext);
	}


	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View view = mInflater.inflate(R.layout.item_list_adapter, parent, false);
		return new ViewHolder(view);
	}

	@Override
	public void onBindViewHolder(ViewHolder holder, int position) {
		Item i = mItems.get(position);
		holder.bind(i);
	}

	@Override
	public int getItemCount() {
		return mItems.size();
	}

	public static class ViewHolder extends RecyclerView.ViewHolder {

		ItemMvvmActivityBinding binding;

		public ViewHolder(View itemView) {
			super(itemView);
			binding = DataBindingUtil.bind(itemView);
		}

		public void bind(Item item) {
//			binding.setVariable(BR.item, item);
			binding.executePendingBindings();
		}
	}
}
