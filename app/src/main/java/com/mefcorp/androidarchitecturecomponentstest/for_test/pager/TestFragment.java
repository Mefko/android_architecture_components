package com.mefcorp.androidarchitecturecomponentstest.for_test.pager;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mefcorp.androidarchitecturecomponentstest.R;

public class TestFragment extends Fragment {


	private static final String ARGS_ID = "ARGS_ID";

	public static TestFragment getInstance(int id) {
		id = id * 1000;
		TestFragment frg = new TestFragment();
		frg.mId = id;
		Bundle args = new Bundle();
		args.putInt(ARGS_ID, id);
		frg.setArguments(args);
		return frg;
	}

	private TextView mText;
	private int mId;

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (savedInstanceState == null) {
			mId = getArguments().getInt(ARGS_ID);
		} else {
			mId = savedInstanceState.getInt(ARGS_ID);
		}
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.frg_test, null);
		mText = view.findViewById(R.id.frg_test_text_view);
		mText.setText(String.valueOf(mId));
		view.findViewById(R.id.frg_test_scroll_view).setFocusableInTouchMode(true);
		LinearLayout scrollArea = view.findViewById(R.id.frg_test_scroll_area);
		for (int i = 0; i < 100; i++) {
			TextView textView = new TextView(getContext());
			textView.setText("Id: " + mId + " item: " + (i + 1));
			textView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
			scrollArea.addView(textView);
		}
		return view;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		outState.putInt(ARGS_ID, mId);
		super.onSaveInstanceState(outState);
	}

	public String getMyTag() {
		return String.valueOf(mId);
	}
}
