package com.mefcorp.androidarchitecturecomponentstest.for_test;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import com.mefcorp.androidarchitecturecomponentstest.R;

import java.util.UUID;

public class TestingDialogFragment extends DialogFragment {


	private ISome mListener;
	private String id;

	public TestingDialogFragment() {
		id = UUID.randomUUID().toString();
		Log.e("TEST", "create: " + id);
	}

	@NonNull
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		View v = LayoutInflater.from(getContext()).inflate(R.layout.for_test2, null);
		v.findViewById(R.id.button).setOnClickListener((view) -> {
			if (mListener != null) {
				Log.e("TEST", "click in: " + id);
				mListener.onClick(id);
			}
		});
//		RadioGroup radiobaton = v.findViewById(R.id.radioButton);
//		RadioButton button;
//		for(int i = 0; i < 1; i++) {
//			button = new RadioButton(getContext());
//			button.setText("Button " + i);
//			radiobaton.addView(button);
//		}
//		RecyclerView list = v.findViewById(R.id.list);
//		list.setLayoutManager(new LinearLayoutManager(getContext()));
//		list.setAdapter(new WithoutPatternsActivity.ListAdapter(1, -1));
		AlertDialog.Builder b = new AlertDialog.Builder(getContext());
		b.setView(v);
		return b.create();
	}

	public void setListener(ISome listener) {
		mListener = listener;
	}

	public interface ISome {

		void onClick(String id);

	}


}
/*
<?xml version="1.0" encoding="utf-8"?>
<android.support.constraint.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent">

    <android.support.v7.widget.RecyclerView
        android:id="@+id/recyclerView"
        android:layout_width="0dp"
        android:layout_height="wrap_content"
        android:layout_marginBottom="8dp"
        android:layout_marginEnd="8dp"
        android:layout_marginStart="8dp"
        android:layout_marginTop="8dp"
        app:layout_constraintBottom_toTopOf="@+id/guideline2"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toTopOf="parent"
        app:layout_constraintVertical_bias="0.0" />

    <Button
        android:id="@+id/button2"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_marginBottom="8dp"
        android:layout_marginEnd="8dp"
        android:layout_marginStart="8dp"
        android:layout_marginTop="8dp"
        android:text="Button"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toBottomOf="@+id/recyclerView"
        app:layout_constraintVertical_bias="0.0" />

    <android.support.constraint.Guideline
        android:id="@+id/guideline2"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:orientation="horizontal"
        app:layout_constraintGuide_end="56dp" />
</android.support.constraint.ConstraintLayout>
 */