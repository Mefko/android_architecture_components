package com.mefcorp.androidarchitecturecomponentstest.for_test.mvvm;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.mefcorp.androidarchitecturecomponentstest.R;

public class MVVMForTestActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle("MVVM");
		setContentView(R.layout.activity_mvvm);
	}
}
