package com.mefcorp.androidarchitecturecomponentstest.for_test.pager;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mefcorp.androidarchitecturecomponentstest.R;

public class PagerFragment extends Fragment {

	public static final String TAG = "queue_sync.PagerFragment";

	public static PagerFragment getInstance() {
		return new PagerFragment();
	}

	private ViewPager mPager;

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.frg_pager, null);
		mPager = view.findViewById(R.id.frg_pager_view_pager);
//		mPager.setOffscreenPageLimit(0);
		mPager.setAdapter(new PagerAdapter(getFragmentManager()));
		return view;
	}


}
