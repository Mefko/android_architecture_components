package com.mefcorp.androidarchitecturecomponentstest.for_test;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.ArrayMap;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.mefcorp.androidarchitecturecomponentstest.ActionLiveData.TestViewModel;
import com.mefcorp.androidarchitecturecomponentstest.R;
import com.mefcorp.androidarchitecturecomponentstest.for_test.pager.PagerFragment;

import java.util.ArrayList;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ForTestActivity extends AppCompatActivity implements View.OnClickListener {


	private TestViewModel mViewModel;
	private ArrayMap<KeyClass, String> mMap = new ArrayMap<>();
	private Observer<Boolean> observer = new Observer<Boolean>() {
		@Override
		public void onChanged(@Nullable Boolean aBoolean) {
			Toast.makeText(getApplicationContext(), "Show", Toast.LENGTH_SHORT).show();
		}
	};
	private MutableLiveData<String> mObsString = new MutableLiveData<>();
	private static ScheduledThreadPoolExecutor mThreadExecutor;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (mThreadExecutor == null) {
			mThreadExecutor = (ScheduledThreadPoolExecutor) Executors.newScheduledThreadPool(1);
		}
		setContentView(R.layout.for_test);
		mViewModel = ViewModelProviders.of(this).get(TestViewModel.class);
		findViewById(R.id.button).setOnClickListener(this);
		findViewById(R.id.button2).setOnClickListener(this);
		mObsString.observe(this, (msg) -> showSome(msg));
//		mViewModel.getActionLiveData().observe(this, observer);
//		for (int i = 0; i < 10; i++) {
//			KeyClass a = new KeyClass();
//			a.number = 1;
//			String a5 = "";
//			mMap.put(a, "is: " + i);
//		}


		initFragment();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.button:
				final UUID uid = UUID.randomUUID();
				showSome(" Start thread id: " + uid.toString());
				BlockingQueue<Runnable> queue = mThreadExecutor.getQueue();
				if (queue != null && queue.size() > 0) {
					queue.clear();
					mObsString.postValue("clear");
				}
				ScheduledFuture<?> t = mThreadExecutor.schedule(() -> {
					String msg = "Thread: " + Thread.currentThread().getId() + " id: " + uid.toString();
					String msg2 = "";
					Log.e("MyF", "start");
					mObsString.postValue(msg + " start execute");
//					try {
//						Thread.sleep(3000);
//					} catch (InterruptedException e) {
//						e.printStackTrace();
//					}
					for (int i = 0; i < 1000000; i++) {
						try {
							if (msg2.length() > 1000) {
								msg2 = "";
							}
							msg2 += String.valueOf(123 * i);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					Log.e("MyF", "end");
					mObsString.postValue(msg + " end execute");
				}, 2000, TimeUnit.MILLISECONDS);

//				TestingDialogFragment fragment = new TestingDialogFragment();
//				fragment.setListener((id) -> Log.e("TEST", "callback " + id));
//				fragment.show(getSupportFragmentManager(), "My");
				break;
			case R.id.button2:
				ArrayList<ArrayList<String>> arrayList = new ArrayList<>();
				arrayList.add(null);
				arrayList.get(0).size();
//				if (mViewModel.getActionLiveData().hasObservers()) {
//					mViewModel.getActionLiveData().removeObservers(this);
//				} else {
//					mViewModel.getActionLiveData().observe(this, observer);
//				}
				break;
		}

	}

	private void initFragment() {
		FragmentManager fm = getSupportFragmentManager();
		Fragment frg = fm.findFragmentByTag(PagerFragment.TAG);
		if (frg == null) {
			frg = PagerFragment.getInstance();
			fm.beginTransaction().replace(R.id.for_test_frame, frg, PagerFragment.TAG).commit();
		}
	}

	private void showSome(String msg) {
		Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
		Log.e("MyF", msg);
	}

	private class KeyClass {
		int number;
		int hash = 1;
		Random r = new Random();

		@Override
		public int hashCode() {
			Log.e("My", "hashCode");
			return hash;//r.nextInt(4);
		}

		@Override
		public boolean equals(Object obj) {
			Log.e("My", "equals");
			if (obj == this) {
				return true;
			}
			if (obj != null && obj instanceof KeyClass) {
				KeyClass a = (KeyClass) obj;
				return false;//a.number == number;
			}
			return false;
		}
	}

}
