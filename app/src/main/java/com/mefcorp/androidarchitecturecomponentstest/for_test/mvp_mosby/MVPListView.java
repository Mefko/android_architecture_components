package com.mefcorp.androidarchitecturecomponentstest.for_test.mvp_mosby;

import com.hannesdorfmann.mosby.mvp.lce.MvpLceView;

public interface MVPListView extends MvpLceView<MVPListPresenter.ForTestData> {
}
