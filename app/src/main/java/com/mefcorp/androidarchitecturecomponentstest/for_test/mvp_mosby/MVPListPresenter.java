package com.mefcorp.androidarchitecturecomponentstest.for_test.mvp_mosby;

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import java.util.ArrayList;

public class MVPListPresenter extends MvpBasePresenter<MVPListView> {

	private ForTestData mDataProvider;

	public MVPListPresenter() {
		mDataProvider = new ForTestData();
	}

	public void loadList() {

		if (isViewAttached()) {
			if (mDataProvider.getData().size() > 0) {
				getView().setData(mDataProvider);
				getView().showContent();
			} else {
				getView().showError(new Exception("Data is empty"), false);
			}
		}

	}

	public static class ForTestData {
		private ArrayList<String> getData() {
			int count = 10;
			ArrayList<String> data = new ArrayList<>(count);
			for (int i = 1; i <= count; i++) {
				data.add("Items: " + i);
			}
			return data;
		}
	}
}
