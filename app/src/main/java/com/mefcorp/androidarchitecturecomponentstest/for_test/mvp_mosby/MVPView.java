package com.mefcorp.androidarchitecturecomponentstest.for_test.mvp_mosby;

import com.hannesdorfmann.mosby.mvp.MvpView;

public interface MVPView extends MvpView{

	void showHello(String text);

	void showGoodbye(String text);
}
