package com.mefcorp.androidarchitecturecomponentstest.for_test.mvp_mosby;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.widget.TextView;

import com.hannesdorfmann.mosby.mvp.MvpActivity;
import com.mefcorp.androidarchitecturecomponentstest.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MVPActivity extends MvpActivity<MVPView, MVPPresenter> implements MVPView {

	@BindView(R.id.act_mvp_pattern_text)
	TextView mTextView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_mvp_pattetn);
		ButterKnife.bind(this);
	}

	@NonNull
	@Override
	public MVPPresenter createPresenter() {
		return new MVPPresenter();
	}

	@OnClick(R.id.act_mvp_pattern_text)
	void onClickHello() {
		presenter.greetHello();
	}

	@OnClick(R.id.act_mvp_pattern_goodbye)
	void onClickGoodbye() {
		presenter.greetGoodbye();
	}

	@Override
	public void showHello(String text) {
		mTextView.setTextColor(Color.RED);
		mTextView.setText(text);
	}

	@Override
	public void showGoodbye(String text) {
		mTextView.setTextColor(Color.GREEN);
		mTextView.setText(text);
	}
}
