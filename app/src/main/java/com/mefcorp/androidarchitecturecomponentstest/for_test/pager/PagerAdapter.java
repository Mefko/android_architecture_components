package com.mefcorp.androidarchitecturecomponentstest.for_test.pager;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class PagerAdapter extends android.support.v4.view.PagerAdapter {


	private ArrayList<TestFragment> mFragmets = new ArrayList<>();
	private FragmentManager mFM;
	private FragmentTransaction mCurTransaction = null;

	public PagerAdapter(FragmentManager fm) {
		mFM = fm;
		for (int i = 0; i < 5; i++) {
			mFragmets.add(TestFragment.getInstance(i));
		}
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		TestFragment toInit = mFragmets.get(position);
		Fragment frg = mFM.findFragmentByTag(toInit.getMyTag());
		if (frg == null) {
			if (mCurTransaction == null) {
				mCurTransaction = mFM.beginTransaction();
			}
			mCurTransaction.add(container.getId(), toInit, toInit.getMyTag());
		}
		return toInit;
	}

	@Override
	public void finishUpdate(ViewGroup container) {
		if (mCurTransaction != null) {
			mCurTransaction.commit();
			mCurTransaction = null;
		}
		super.finishUpdate(container);
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		Fragment fragment = (Fragment) object;
		if (mCurTransaction == null) {
			mCurTransaction = mFM.beginTransaction();
		}
		mCurTransaction.remove(fragment);
	}

	@Override
	public int getCount() {
		return mFragmets.size();
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return mFragmets.get(position).getMyTag();
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return ((Fragment) object).getView() == view;
	}
}
