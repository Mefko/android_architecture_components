package com.mefcorp.androidarchitecturecomponentstest;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
	@Test
	public void annotation_testing1() throws Exception {

		System.out.println("test1");
		Assert.assertTrue(true);
	}

	@Test
	public void annotation_testing2() throws Exception {

		System.out.println("test2");
		Assert.assertTrue(true);
	}

	@Before
	public void before_test(){
		System.out.println("Before");
	}

	@After
	public void after_test(){
		System.out.println("After");
	}

	@BeforeClass
	public static void beforeClass_test(){
		System.out.println("BeforeClass");
	}

	@AfterClass
	public static void afterClass_test(){
		System.out.println("AfterClass");
	}
}