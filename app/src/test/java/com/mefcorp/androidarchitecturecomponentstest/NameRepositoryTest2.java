package com.mefcorp.androidarchitecturecomponentstest;

import com.mefcorp.androidarchitecturecomponentstest.for_unit_test.NameRepository;
import com.mefcorp.androidarchitecturecomponentstest.rules.CreateFileRule;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;

import java.io.File;

public class NameRepositoryTest2 {

	private static final File FILE = new File("test_file");
	@Rule
	public final CreateFileRule mFileRule =
			new CreateFileRule(FILE, "{name : Sergiy}");


	@Test
	public void getName_Check_isSergiy() throws Exception {
		//Arrange
		NameRepository repository = new NameRepository(FILE);
		//Act
		String name = repository.getName();
		//Assert
		Assert.assertEquals(name, "Sergiy");
	}

	@Test
	public void getName_Check_notMary() throws Exception {
		//Arrange
		NameRepository repository = new NameRepository(FILE);

		//Act
		String name = repository.getName();

		//Assert
		Assert.assertNotEquals(name, "Mary");
	}
}
