package com.mefcorp.androidarchitecturecomponentstest.stub;

import com.mefcorp.androidarchitecturecomponentstest.for_unit_test.IFileReader;

import java.io.IOException;

public class FileReaderStub implements IFileReader {

	private static final String DATA = "{name : Sergiy}";

	@Override
	public String readData() throws IOException {
		return DATA;
	}

	@Override
	public long fileLength() {
		return DATA.length();
	}
}
