package com.mefcorp.androidarchitecturecomponentstest;

import com.mefcorp.androidarchitecturecomponentstest.for_unit_test.IFileReader;
import com.mefcorp.androidarchitecturecomponentstest.for_unit_test.NameRepository1;
import com.mefcorp.androidarchitecturecomponentstest.stub.FileReaderStub;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class NameRepositoryTest5 {

	IFileReader dataReader = mock(IFileReader.class);

	@Before
	public void setUp() throws IOException {
		String data = "{name : Sergiy}";
		when(dataReader.fileLength()).thenReturn((long)data.length());
		when(dataReader.readData()).thenReturn(data);
	}

	@Test
	public void getName_Check_isSergiy() throws Exception {
		//Arrange
		NameRepository1 repository = new NameRepository1(dataReader);
		//Act
		String name = repository.getName();
		//Assert
		Assert.assertEquals(name, "Sergiy");
	}

	@Test
	public void getName_fileLength_called() throws Exception {
		//Arrange
		NameRepository1 repository = new NameRepository1(dataReader);
		//Act
		repository.getName();
		//Assert
		verify(dataReader, times(1)).fileLength();
	}

	@Test
	public void getName_Check_notMary() throws Exception {
		//Arrange
		IFileReader dataReader = new FileReaderStub();
		NameRepository1 repository = new NameRepository1(dataReader);

		//Act
		String name = repository.getName();

		//Assert
		Assert.assertNotEquals(name, "Mary");
	}
}
