package com.mefcorp.androidarchitecturecomponentstest;

import com.mefcorp.androidarchitecturecomponentstest.for_unit_test.IFileReader;
import com.mefcorp.androidarchitecturecomponentstest.for_unit_test.NameRepository1;
import com.mefcorp.androidarchitecturecomponentstest.stub.FileReaderStub;

import org.junit.Assert;
import org.junit.Test;

public class NameRepositoryTest4 {

	@Test
	public void getName_Check_isSergiy() throws Exception {
		//Arrange
		IFileReader dataReader = new FileReaderStub();
		NameRepository1 repository = new NameRepository1(dataReader);
		//Act
		String name = repository.getName();
		//Assert
		Assert.assertEquals(name, "Sergiy");
	}

	@Test
	public void getName_Check_notMary() throws Exception {
		//Arrange
		IFileReader dataReader = new FileReaderStub();
		NameRepository1 repository = new NameRepository1(dataReader);

		//Act
		String name = repository.getName();

		//Assert
		Assert.assertNotEquals(name, "Mary");
	}
}
