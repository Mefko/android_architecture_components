package com.mefcorp.androidarchitecturecomponentstest.rules;

import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import java.io.File;

public class CreateDirRule implements TestRule {

	private final File dir;

	/**
	 *
	 * @param dir - dir that will be created.
	 */
	public CreateDirRule(File dir) {
		this.dir = dir;
	}


	@Override
	public Statement apply(Statement base, Description description) {
		return new Statement() {
			@Override
			public void evaluate() throws Throwable {
				System.out.println("dss");
				dir.mkdir();
				try {
					base.evaluate();
				} finally {
					dir.delete();
				}
			}
		};
	}
}
