package com.mefcorp.androidarchitecturecomponentstest.rules;

import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

public class CreateFileRule implements TestRule {
	private final File mFile;
	private final String mText;

	/**
	 * @param file - file that will be created.
	 * @param text - created file content.
	 */
	public CreateFileRule(File file, String text) {
		this.mFile = file;
		this.mText = text;
	}

	@Override
	public Statement apply(final Statement base, Description description) {
		return new Statement() {
			@Override
			public void evaluate() throws Throwable {
				PrintWriter writer =
						new PrintWriter(
								new BufferedWriter(new OutputStreamWriter(new FileOutputStream(mFile), "UTF-8")),
								true);
				writer.println(mText);
				writer.close();
				try {
					base.evaluate();
				} finally {
					mFile.delete();
				}
			}
		};
	}
}
