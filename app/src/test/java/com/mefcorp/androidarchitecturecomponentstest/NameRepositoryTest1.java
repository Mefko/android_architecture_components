package com.mefcorp.androidarchitecturecomponentstest;

import com.mefcorp.androidarchitecturecomponentstest.for_unit_test.NameRepository;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

public class NameRepositoryTest1 {

	private static final File FILE = new File("test_file");

	@Before
	public void setUp() throws Exception {
		//Arrange
		PrintWriter writer = new PrintWriter(
				new BufferedWriter(
						new OutputStreamWriter(new FileOutputStream(FILE), "UTF-8")), true);
		writer.println("{name : Sergiy}");
		writer.close();
	}

	@After
	public void tearDown() {
		FILE.delete();
	}

	@Test
	public void getName_Check_isSergiy() throws Exception {
		//Arrange
		NameRepository repository = new NameRepository(FILE);
		//Act
		String name = repository.getName();
		//Assert
		Assert.assertEquals(name, "Sergiy");
	}

	@Test
	public void getName_Check_notMary() throws Exception {
		//Arrange
		NameRepository repository = new NameRepository(FILE);
		//Act
		String name = repository.getName();

		//Assert
		Assert.assertNotEquals(name, "Mary");
	}
}
